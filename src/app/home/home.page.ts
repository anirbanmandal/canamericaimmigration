import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  slidesPerView: any;

  constructor() {}

  async ionViewWillEnter() {
    this.resizeSwiper();

  }
  swiperSlideChanged(e: any) {
    // console.log('changed: ', e);
  }

  async resizeSwiper() {
    const screenWidth = window.innerWidth;
    if (screenWidth < 420) {
      this.slidesPerView = 1;
    } else if (screenWidth >= 1200) {
      this.slidesPerView = 3;
    }
  }

}
